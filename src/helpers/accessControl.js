import React, { Component } from "react"
import { connect } from "react-redux"

export const accessControl = permissionsRequired => WrappedComponent => {
  const SecuredControl = class extends Component {
    render() {
      const { permissions } = this.props.user
      const isAllow = permissionsRequired.every(permission => permissions.indexOf(permission) >= 0)

      if (!isAllow) {
        return (
          <div>
            <i>You are not allowed</i>
          </div>
        )
      }
      return <WrappedComponent { ...this.props } />
    }
  }

  return connect(state => ({
    user: state.user
  }))(SecuredControl)
}