import React, { Component } from "react";

export const setPropsAsInitial = WrappedComponent => (
  class extends Component {
    render() {
      return (
        <WrappedComponent
          { ...this.props }
          initialValues={ this.props } // Only is initialized the first time
          enableReinitialize // Allows to re-initialize the values on refresh.
        />
      )
    }
  }
)