import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import './styles/colors.css';
import Home from './containers/Home';
import CustomersContainer from './containers/CustomersContainer';
import CustomerContainer from './containers/CustomerContainer'
import NewCustomerContainer from './containers/NewCustomerContainer';

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={ Home } />
        <Route exact path="/customers" component={ CustomersContainer } />
        <Switch>
          <Route path="/customers/new" component={ NewCustomerContainer } />
          <Route path="/customers/:id" render={ props => <CustomerContainer id={ props.match.params.id } /> } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
