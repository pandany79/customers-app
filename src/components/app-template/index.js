import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Footer from './Footer';

const AppTemplate = ({ header, body, footer }) => {
  return (
    <div className="app-template">
      <Header title={ header } />
      <div>
        { body }
      </div>
      <Footer />
    </div>
  );
};

AppTemplate.propTypes = {
  header: PropTypes.string.isRequired,
  body: PropTypes.element.isRequired,
};

export default AppTemplate;