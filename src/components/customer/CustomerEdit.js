import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { reduxForm, Field } from 'redux-form'
import { setPropsAsInitial } from '../../helpers/setPropsAdInitial';
import CustomersActions from './CustomersActions';
import { Prompt } from 'react-router-dom';
import { accessControl } from '../../helpers/accessControl';
import { CUSTOMER_EDIT } from '../../constants/permissions';

const isNumber = value => (
  isNaN(Number(value)) && 'The field must be a number'
)

const validate = values => {
  const error = {}

  if (!values.name) {
    error.name = 'Field Name is required'
  }

  if (!values.code) {
    error.code = 'Field ID is required'
  }

  return error
}

const toNumber = value => value && Number(value)

const toUpper = value => value && value.toUpperCase()

const toLower = value => value && value.toLowerCase()

const onlyGrow = (value, previousValue, values) =>
  value && (!previousValue ? value : (value > previousValue ? value : previousValue))

class CustomerEdit extends Component {
  componentDidMount() {
    if (this.inputText) {
      this.inputText.focus()
    }
  }

  renderField = ({ input, meta, type, label, name, withFocus }) => (
    <div>
      <label htmlFor={ name }>{ label }</label>
      <input
        {...input}
        type={ !type ? 'text' : type }
        ref={ withFocus && (inputText => this.inputText = inputText) }
      />
      {
        meta.touched && meta.error && <span>{ meta.error }</span>
      }
    </div>
  )

  render() {
    const { handleSubmit, submitting, onBack, pristine, submitSucceeded } = this.props
    return (
      <div>
        <h2>Edit Customer</h2>
        <form onSubmit={ handleSubmit }>
          <Field
            withFocus
            name="name"
            label="Name:"
            component={ this.renderField }
            parse={ toUpper }
            format={ toLower }
          />
          <Field
            name="code"
            label="Code:"
            component={ this.renderField }
          />
          <Field
            type="number"
            name="age"
            label="Age:"
            component={ this.renderField }
            validate={ isNumber }
            parse={ toNumber }
            normalize={ onlyGrow }
          />
          <CustomersActions>
            <button
              type="submit"
              disabled={ pristine || submitting }
            >
              Save
            </button>
            <button
              type="button"
              onClick={ onBack }
              disabled={ submitting }
            >
              Cancel
            </button>
          </CustomersActions>
          <Prompt
            when={ !pristine && !submitSucceeded }
            message="Changes will be lost if continue"
          ></Prompt>
        </form>
      </div>
    );
  }
}

CustomerEdit.propTypes = {
  name: PropTypes.string,
  code: PropTypes.string,
  age: PropTypes.number,
  onBack: PropTypes.func.isRequired,
};

const CustomerEditForm = reduxForm({
  form: 'CustomerEdit',
  validate
})(CustomerEdit)

// Use the Higher Order Component (HOC) "setPropsAsInitial".
export default accessControl([CUSTOMER_EDIT])(setPropsAsInitial(CustomerEditForm))