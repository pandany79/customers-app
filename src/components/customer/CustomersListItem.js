import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const CustomersListItem = ({ name, id, editAction, deleteAction, urlPath }) => {
  return (
    <div className="customers-list-item">
      <div className="field">
        <Link to={ `${urlPath}${id}` }>{ name }</Link>
      </div>
      <div className="field">
        <Link to={ `${urlPath}${id}/edit` }>{ editAction }</Link>
      </div>
      <div className="field">
        <Link to={ `${urlPath}${id}/delete` }>{ deleteAction }</Link>
      </div>
    </div>
  );
};

CustomersListItem.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  editAction: PropTypes.string.isRequired,
  deleteAction: PropTypes.string.isRequired,
  urlPath: PropTypes.string.isRequired,
};

export default CustomersListItem;