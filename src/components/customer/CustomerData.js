import React from 'react';
import PropTypes from 'prop-types';
import CustomersActions from './CustomersActions';
import { CUSTOMER_VIEW } from '../../constants/permissions';
import { accessControl } from '../../helpers/accessControl';

const CustomerData = ({ id, name, code, age, onBack, isDeleteAllow, onDelete }) => {
  return (
    <div className="customer-data">
      <h2>Customers Data</h2>
      <div className="header">
        Name: 
        <span>{ name }</span>
      </div>
      <div className="header">
        Code: 
        <span>{ code }</span>
      </div>
      <div className="header">
        Age: 
        <span>{ age }</span>
      </div>
      <CustomersActions>
        <button onClick={ onBack }>
          Go Back
        </button>
        {
          isDeleteAllow && (
            <button onClick={ () => onDelete(id) }>
              Delete
            </button>
          )
        }
      </CustomersActions>
    </div>
  );
};

CustomerData.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  code: PropTypes.string.isRequired,
  age: PropTypes.number,
  onBack: PropTypes.func.isRequired,
  isDeleteAllow: PropTypes.bool.isRequired,
  onDelete: PropTypes.func,
};

export default accessControl([CUSTOMER_VIEW])(CustomerData)