import React from 'react'
import PropTypes from 'prop-types'
import CustomerListItem from './CustomersListItem'
import { accessControl } from '../../helpers/accessControl'
import { CUSTOMER_LIST } from '../../constants/permissions'

const CustomersList = ({ customers, urlPath }) => {
  return (
    <div className="customers-list">
      {
        customers.map(customer =>
          <CustomerListItem
            key={ customer.id }
            id={ customer.id }
            name={ customer.name }
            editAction='Edit'
            deleteAction='Delete'
            urlPath={ urlPath }
          />
        )
      }
    </div>
  )
}

CustomersList.propTypes = {
  customers: PropTypes.array.isRequired,
  urlPath: PropTypes.string.isRequired,
}

export default accessControl([CUSTOMER_LIST])(CustomersList)