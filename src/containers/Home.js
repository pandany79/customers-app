import React from 'react';
import { withRouter } from 'react-router-dom';
import AppTemplate from '../components/app-template';
import CustomersActions from '../components/customer/CustomersActions';


const Home = props => {
  const onClick = () => {
    props.history.push('/customers')
  }

  return (
    <div>
      <AppTemplate
        header='Home'
        body={
          <div>
            Initial Page
            <CustomersActions>
              <button onClick={ onClick }>
                Customers List
              </button>
            </CustomersActions>
          </div>
        }
      />
    </div>
  );
};

export default withRouter(Home);