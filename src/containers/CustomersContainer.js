import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import AppTemplate from '../components/app-template'
import CustomersList from '../components/customer/CustomersList'
import CustomersActions from '../components/customer/CustomersActions'
import { fetchCustomers } from './../actions/fetchCustomers'
import { getCustomers } from './../selectors/customers'

const CustomersContainer = props => {
  useEffect(() => {
    if (!props.customers.length) {
      props.fetchCustomers();
    }
  }, [props])

  const onAddNew = () => {
    props.history.push('/customers/new')
  }

  const renderBody = (customers) => (
    <div>
      <CustomersList
        customers={ customers }
        urlPath={ 'customers/' }
      />
      <CustomersActions>
        <button onClick={ onAddNew }>
          New Customer
        </button>
      </CustomersActions>
    </div>
  )

  return (
    <div>
      <AppTemplate
        header='Customers List'
        body={
          renderBody(props.customers)
        }
      />
    </div>
  );
};

CustomersContainer.propTypes = {
  fetchCustomers: PropTypes.func.isRequired,
  customers: PropTypes.array.isRequired,
};

CustomersContainer.defaultProps = {
  customers: []
}

const mapStateToProps = state => ({
  customers: getCustomers(state)
})

export default withRouter(connect(mapStateToProps, { fetchCustomers })(CustomersContainer));