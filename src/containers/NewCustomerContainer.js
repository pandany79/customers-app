import React from 'react'
import PropTypes from 'prop-types'
import AppTemplate from '../components/app-template'
import CustomerEdit from '../components/customer/CustomerEdit'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { insertCustomer } from './../actions/insertCustomer'
import { SubmissionError } from 'redux-form'

const NewCustomerContainer = props => {
  const handleSubmit = values => {
    return props.insertCustomer(values)
      .then(result => {
        if (result.error) {
          throw new SubmissionError(result.payload)
        }
      })
  }

  const handleSubmitSuccess = () => {
    props.history.goBack()
  }

  const handleBack = () => {
    props.history.goBack()
  }

  const renderBody = () => {
    return (
      <CustomerEdit
        onSubmit={ handleSubmit }
        onSubmitSuccess={ handleSubmitSuccess }
        onBack={ handleBack }
      />
    )
  }

  return (
    <div>
      <AppTemplate
        header={`New Client`}
        body={
          renderBody()
        }
      />
    </div>
  );
};

NewCustomerContainer.propTypes = {
  insertCustomer: PropTypes.func.isRequired,
};

export default withRouter(connect(null, { insertCustomer })(NewCustomerContainer))