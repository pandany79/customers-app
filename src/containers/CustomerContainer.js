import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import AppTemplate from '../components/app-template'
import { getCustomerById } from '../selectors/customers';
import { Route, withRouter } from 'react-router-dom';
import CustomerEdit from '../components/customer/CustomerEdit';
import CustomerData from '../components/customer/CustomerData';
import { fetchCustomers } from './../actions/fetchCustomers'
import { updateCustomer } from './../actions/updateCustomer'
import { deleteCustomer } from './../actions/deleteCustomer'
import { SubmissionError } from 'redux-form';

const CustomerContainer = props => {
  useEffect(() => {
    if (!props.customer) {
      props.fetchCustomers()
    }
  }, [props])

  const handleSubmit = values => {
    const { id } = values

    // Return the promise for "submitting" status for "CustomerEdit".
    return props.updateCustomer(id, values)
      .then(result => {
        if (result.error) {
          throw new SubmissionError(result.payload)
        }
      })
  }

  const handleBack = () => {
    props.history.goBack()
  }

  const handleSubmitSuccess = () => {
    handleBack()
  }

  const handleDelete = id => {
    props.deleteCustomer(id)
      .then(result => {
        handleBack()
      })
  }

  const renderCustomerControl = (isEdit, isDelete) => {
    if (props.customer) {
      const CustomerControl = isEdit
        ? CustomerEdit
        : CustomerData

      return (
        <CustomerControl
          { ...props.customer }
          onSubmit={ handleSubmit }
          onSubmitSuccess={ handleSubmitSuccess }
          onBack={ handleBack }
          isDeleteAllow={ !!isDelete }
          onDelete={ handleDelete }
        />
      )
    }

    return null
  }

  const renderBody = () => (
    <Route
      path="/customers/:id/edit"
      children={
        ({ match: isEdit }) => (
          <Route
            path="/customers/:id/delete"
            children={
              ({ match: isDelete }) => (
                renderCustomerControl(isEdit, isDelete)
              )
            }
          />
        )
      }
    />
  )

  return (
    <div>
      <AppTemplate
        header={`Customer ${props.id} `}
        body={
          renderBody()
        }
      />
    </div>
  );
};

CustomerContainer.propTypes = {
  id: PropTypes.string.isRequired,
  customer: PropTypes.object,
  fetchCustomers: PropTypes.func.isRequired,
  updateCustomer: PropTypes.func.isRequired,
  deleteCustomer: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
  customer: getCustomerById(state, props)
})

export default withRouter(connect(
  mapStateToProps, {
    fetchCustomers,
    updateCustomer,
    deleteCustomer
  }
)(CustomerContainer))