export const apiGet = (apiUrl) => () => fetch(apiUrl).then(result => result.json())

export const apiPut = (apiUrl, id, customer) => () =>
  fetch(`${apiUrl}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(customer),
    headers: new Headers({ 'Content-type': 'application/json' })
  })
    .then(result => result.json())
    .then(data => {
      if (data.error) {
        return Promise.reject(data.validation)
      }
      return data
    })

export const apiPost = (apiUrl, customer) => () =>
  fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify(customer),
    headers: new Headers({ 'Content-type': 'application/json' })
  })
    .then(result => result.json())
    .then(data => {
      if (data.error) {
        return Promise.reject(data.validation)
      }
      return data
    })

export const apiDelete = (apiUrl, id) => () =>
  fetch(`${apiUrl}/${id}`, {
    method: 'DELETE',
    headers: new Headers({ 'Content-type': 'application/json' })
  })
    .then(result => result.json())
    .then(data => {
      if (data.error) {
        return Promise.reject(data.validation)
      }
      return id
    })