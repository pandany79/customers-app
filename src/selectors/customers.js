import { createSelector } from 'reselect'

export const getCustomers = state => state.customers

export const getCustomerById = createSelector(
  (state, props) => state.customers.find(customer => customer.id === props.id),
  customer => customer
)